"""kobic_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from kobic_snuh_diabetes.views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^index/', index, name='index'),
    url(r'^show_search/(.*)', show_search, name='show_search'),
    url(r'^show_notice/', show_notice, name='show_notice'),
    url(r'^show_contact_us/', show_contact_us, name='show_contact_us'),
    url(r'^show_search_result/(\d+)/(\d+)/(\d+)/$', show_search_result, name='show_search_result'),
    url(r'^export_xlsx/(\w+)/(\d+)/(\d+)', export_xlsx, name='export_xlsx'),
    url(r'^admin/', admin.site.urls),
]
