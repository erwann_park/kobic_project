# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-09-22 05:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kobic_snuh_diabetes', '0007_kobicsnuhdiabetesrs10'),
    ]

    operations = [
        migrations.CreateModel(
            name='InputGenesText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('input_genes_text', models.TextField(blank=True)),
            ],
        ),
    ]
