# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-09-06 07:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kobic_snuh_diabetes', '0003_inputsnpidtext'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='InputSnpidText',
            new_name='InputSnpidsText',
        ),
        migrations.RenameField(
            model_name='inputsnpidstext',
            old_name='input_snpid_text',
            new_name='input_snpids_text',
        ),
    ]
