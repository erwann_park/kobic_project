from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.encoding import smart_str

from kobic_snuh_diabetes.models import *
from collections import OrderedDict

import pickle
import json
import sys
import numpy as np
import random
import math
import csv
import dropbox
import time
import datetime
import re
import csv 
import xlsxwriter
import random
from datetime import datetime


import urllib
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2

class GlobalManager:
    
    @staticmethod
    def get_result_object_list(_id):
        return result_object_list_dict[int(_id)]
    @staticmethod
    def set_result_object_list(_id, _result_object_list):
        global result_object_list_dict

        if'result_object_list_dict' not in globals():
        	result_object_list_dict = {_id : _result_object_list}
        else:
            result_object_list_dict[_id] = _result_object_list

    @staticmethod
    def get_total_page_number(_id):
        return total_page_number_dict[int(_id)]
    @staticmethod
    def set_total_page_number(_id, _total_page_number):
        global total_page_number_dict

        if'total_page_number_dict' not in globals():
        	total_page_number_dict = {_id : _total_page_number}
        else:
            total_page_number_dict[_id] = _total_page_number

    @staticmethod
    def get_current_page_number(_id):
        return current_page_number_dict[int(_id)]
    @staticmethod
    def set_current_page_number(_id, _current_page_number):
        global current_page_number_dict

        if'current_page_number_dict' not in globals():
        	current_page_number_dict = {_id : _current_page_number}
        else:
            current_page_number_dict[_id] = _current_page_number

    @staticmethod
    def get_search_option(_id):
        return search_option_dict[int(_id)]
    @staticmethod
    def set_search_option(_id, _search_option):
        global search_option_dict

        if'search_option_dict' not in globals():
            search_option_dict = {_id : _search_option}
        else:
            search_option_dict[_id] = _search_option

    @staticmethod
    def get_raw_input_text(_id):
        return raw_input_text_dict[int(_id)]
    @staticmethod
    def set_raw_input_text(_id, _raw_input_text):
        global raw_input_text_dict

        if'raw_input_text_dict' not in globals():
            raw_input_text_dict = {_id : _raw_input_text}
        else:
            raw_input_text_dict[_id] = _raw_input_text

    @staticmethod
    def get_previous_database(_id):
        return previous_database_dict[int(_id)]
    @staticmethod
    def set_previous_database(_id, _previous_database):
        global previous_database_dict

        if'previous_database_dict' not in globals():
            previous_database_dict = {_id : _previous_database}
        else:
            previous_database_dict[_id] = _previous_database

    @staticmethod
    def get_checked_database_list(_id):
        return checked_database_list_dict[int(_id)]
    @staticmethod
    def set_checked_database_list(_id, _checked_database_list):
        global checked_database_list_dict

        if'checked_database_list_dict' not in globals():
            checked_database_list_dict = {_id : _checked_database_list}
        else:
            checked_database_list_dict[_id] = _checked_database_list

# Create your views here.
def index(_request):
	return render(_request, 'index.html')

def show_search(_request, _search_option):
	if _request.method == 'POST':
		print('POST >> ', _request.POST)
		input_coordinates_text_form = InputCoordinatesTextForm(_request.POST)
		input_snpids_text_form = InputSnpidsTextForm(_request.POST)
		input_genes_text_form = InputGenesTextForm(_request.POST)
		if input_coordinates_text_form.is_valid() and _search_option == 'coordinates':
			raw_input_text = input_coordinates_text_form.cleaned_data['input_coordinates_text']
			raw_checked_database_list = input_coordinates_text_form.cleaned_data['checked_database_list']
			checked_database_list = refine_database_list(raw_checked_database_list)
			result_json_data, hgmd_json_data = search_database(_request, _search_option, raw_input_text, checked_database_list[0])
		elif input_snpids_text_form.is_valid() and _search_option == 'snpids':
			raw_input_text = input_snpids_text_form.cleaned_data['input_snpids_text']
			raw_checked_database_list = input_snpids_text_form.cleaned_data['checked_database_list']
			checked_database_list = refine_database_list(raw_checked_database_list)
			result_json_data, hgmd_json_data = search_database(_request, _search_option, raw_input_text, checked_database_list[0])
		elif input_genes_text_form.is_valid() and _search_option == 'genes':
			raw_input_text = input_genes_text_form.cleaned_data['input_genes_text']
			raw_checked_database_list = input_genes_text_form.cleaned_data['checked_database_list']
			checked_database_list = refine_database_list(raw_checked_database_list)
			result_json_data, hgmd_json_data = search_database(_request, _search_option, raw_input_text, checked_database_list[0])

		current_database = checked_database_list[0]
		random_id = _request.session['random_id']
		total_page_number = GlobalManager.get_total_page_number(random_id)
		current_page_number = GlobalManager.get_current_page_number(random_id)
		GlobalManager.set_search_option(random_id, _search_option)
		GlobalManager.set_raw_input_text(random_id, raw_input_text)
		GlobalManager.set_previous_database(random_id, current_database)
		GlobalManager.set_checked_database_list(random_id, checked_database_list)
		temp_checked_database_list = checked_database_list[:]
		temp_checked_database_list.remove(current_database)
		rest_databases_list = temp_checked_database_list

		return render(_request, 'show_search_result.html', {'raw_input_text' : raw_input_text, 'result_json_data' : result_json_data, 'total_page_number' : total_page_number+1, 'page_number' : current_page_number+1, 'random_id' : random_id, 'current_database': current_database, 'database_list': rest_databases_list, 'hgmd_json_data': hgmd_json_data})
	else:
		print('GET >> ', _request.GET)
		return render(_request, 'show_search.html')

def refine_database_list(_raw_checked_database_list):
	checked_database_list = []
	split_raw_checked_database_list = _raw_checked_database_list.strip().split(',')
	for split_raw_checked_database_list_element in split_raw_checked_database_list:
		regex_search_result_checked_database_list = re.search('"([\d+])":"([a-zA-Z\s\+]+)"', split_raw_checked_database_list_element)
		if regex_search_result_checked_database_list is not None:
			checked_database_list.append([regex_search_result_checked_database_list.group(1),regex_search_result_checked_database_list.group(2)])
	return checked_database_list

def show_notice(_request):
	return render(_request, 'show_notice.html')

def show_contact_us(_request):
	return render(_request, 'show_contact_us.html')

def search_database(_request, _search_option, _raw_input_text, _current_database):
	result_object_list, error_result_list, current_page_number = [], [100, [], []], 0
	random_id = random.randint(1,100000000)
	input_text_list = get_input_text_list(_raw_input_text)

	if _search_option == 'coordinates':
		for input_text_line in input_text_list:
			error_result_list, chr_number_string, user_start_int, user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string = check_chromosome_position(input_text_line, error_result_list)
			if(error_result_list[0] == 100):
				result_object =  query_by_coordinate_in_database[_current_database[0]](chr_number_string, user_start_int, user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string)
				if(result_object is None):
					print("[ Search out of region ]")
				else:
					result_object_list += result_object
	elif _search_option == 'snpids':
		for input_text_line in input_text_list:
			result_object =  query_by_snp_in_database[_current_database[0]](input_text_line)
			if(result_object is None):
				print("[ No Result ]")
			else:
				result_object_list += result_object
	elif _search_option == 'genes':
		for input_text_line in input_text_list:
			result_object =  query_by_gene_in_database[_current_database[0]](input_text_line)
			if(result_object is None):
				print("[ No Result ]")
			else:
				result_object_list += result_object

	_request.session['random_id'] = random_id
	GlobalManager.set_result_object_list(random_id, result_object_list)
	GlobalManager.set_current_page_number(random_id, current_page_number)

	if len(result_object_list) is not 0:
		GlobalManager.set_total_page_number(random_id, (len(result_object_list)-1)//35)
	else:
		GlobalManager.set_total_page_number(random_id, 0)

	if(len(result_object_list)>=35):
		result_json_data = json.dumps(result_object_list[0:35], ensure_ascii=False)
		hgmd_object_list = search_hgmd(result_object_list[0:35], _current_database)
		hgmd_json_data = json.dumps(hgmd_object_list, ensure_ascii=False)
	else:
		result_json_data = json.dumps(result_object_list, ensure_ascii=False)
		hgmd_object_list = search_hgmd(result_object_list, _current_database)
		hgmd_json_data = json.dumps(hgmd_object_list, ensure_ascii=False)
	return result_json_data, hgmd_json_data

def search_hgmd(_result_object_list, _current_database):
	hgmd_result_list = []
	empty_data = { 'NA' }

	for result_object_list_element in _result_object_list:
		if(len(send_query_to_hgmd[_current_database[0]](result_object_list_element).values())!=0):
			hgmd_result_list += send_query_to_hgmd[_current_database[0]](result_object_list_element).values()
		else:
			hgmd_result_list += empty_data

	return hgmd_result_list

def get_input_text_list(_raw_input_text):
    if '\r\n' in _raw_input_text:
        input_text_list = _raw_input_text.strip().split('\r\n')
    elif '\n' in _raw_input_text:
        input_text_list = _raw_input_text.strip().split('\n')
    elif '\r' in _raw_input_text:
        input_text_list = _raw_input_text.strip().split('\r')
    else:
        input_text_list = [_raw_input_text]

    return input_text_list

def show_search_result(_request, _current_database_number, _previous_random_id, _current_page_number):
	previous_database = GlobalManager.get_previous_database(_previous_random_id)

	if previous_database[0] == _current_database_number:
		total_page_number = GlobalManager.get_total_page_number(_previous_random_id)
		result_object_list = GlobalManager.get_result_object_list(_previous_random_id)
		checked_database_list = GlobalManager.get_checked_database_list(_previous_random_id)
		for checked_database_element in checked_database_list:
			if int(_current_database_number) == int(checked_database_element[0]):
				current_database = checked_database_element
		temp_checked_database_list = checked_database_list[:]
		temp_checked_database_list.remove(current_database)
		rest_databases_list = temp_checked_database_list
		current_page_number = int(_current_page_number) -1
		if current_page_number == -1:
			current_page_number = 0
		elif current_page_number == (total_page_number + 1):
			current_page_number = total_page_number
		result_json_data = json.dumps(result_object_list[35*(current_page_number):35*(current_page_number+1)], ensure_ascii=False)
		hgmd_object_list = search_hgmd(result_object_list[35*(current_page_number):35*(current_page_number+1)], previous_database)
		hgmd_json_data = json.dumps(hgmd_object_list, ensure_ascii=False)
		return render(_request, 'show_search_result.html', {'result_json_data': result_json_data, 'page_number': current_page_number+1, 'total_page_number': total_page_number+1, 'random_id' : _previous_random_id, 'current_database': previous_database, 'database_list': rest_databases_list, 'hgmd_json_data': hgmd_json_data})
	elif previous_database[0] != _current_database_number:
		raw_input_text = GlobalManager.get_raw_input_text(_previous_random_id)
		search_option = GlobalManager.get_search_option(_previous_random_id)
		checked_database_list = GlobalManager.get_checked_database_list(_previous_random_id)
		for checked_database_element in checked_database_list:
			if int(_current_database_number) == int(checked_database_element[0]):
				current_database = checked_database_element
		temp_checked_database_list = checked_database_list[:]
		temp_checked_database_list.remove(current_database)
		rest_databases_list = temp_checked_database_list
		result_json_data, hgmd_json_data = search_database(_request, search_option, raw_input_text, current_database[0])
		current_random_id = _request.session['random_id']
		page_number = GlobalManager.get_current_page_number(current_random_id)
		total_page_number = GlobalManager.get_total_page_number(current_random_id)
		GlobalManager.set_search_option(current_random_id, search_option)
		GlobalManager.set_raw_input_text(current_random_id, raw_input_text)
		GlobalManager.set_previous_database(current_random_id, current_database)
		GlobalManager.set_checked_database_list(current_random_id, checked_database_list)
		return render(_request, 'show_search_result.html', {'result_json_data': result_json_data, 'page_number': page_number+1, 'total_page_number': total_page_number+1, 'random_id' : current_random_id, 'current_database': current_database, 'database_list': rest_databases_list, 'hgmd_json_data': hgmd_json_data})
	else:
		_request.session['previous_database'] = _current_database_number
		print("[ Unexpected Error at show_genometrax_results ]")

def change_arguments_into_url(_target, _link_type):
    temp_string = ""
    pubmed_basic_url = "http://www.ncbi.nlm.nih.gov/pubmed/?term="
    uniprot_basic_url = "http://www.uniprot.org/uniprot/"

    if _target == "N/A":
        return _target
    else:
        split_target = _target.split(",")

        if _link_type == "pmid_link":
            for split_target_element in split_target:
                temp_string += pubmed_basic_url + split_target_element + ", "
        elif _link_type == "uniprot_link":
            for split_target_element in split_target:
                temp_string += uniprot_basic_url + split_target_element + ", "
        return temp_string[:len(temp_string)-2]

def get_description_element(_regex, _target):
    description_element = re.search(_regex, _target)
    return urllib2.unquote(description_element.group(1))

def get_database_name(_current_database_number):
	if _current_database_number == '1':
		return 'GWAS + Imputation'
	elif _current_database_number == '2':
		return 'Exome Sequence Data'

def export_xlsx(_request, _downloadStatus, _current_database, _random_id):
	error_result_list = [100]

	response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
	response['Content-Disposition'] = 'attachment; filename=Search_Result.xlsx'
	workbook = xlsxwriter.Workbook(response, {'in_memory': True})
	search_option = GlobalManager.get_search_option(_random_id)
	raw_input_text = GlobalManager.get_raw_input_text(_random_id)
	input_text_list = get_input_text_list(raw_input_text)

	if _downloadStatus == 'all':
		checked_database_list = GlobalManager.get_checked_database_list(_random_id)

		for checked_database_list_element in checked_database_list: 
			result_object_list = []
			for input_text_line in input_text_list:
				if search_option == 'coordinates':
					for input_text_line in input_text_list:
						error_result_list, chr_number_string, user_start_int, user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string = check_chromosome_position(input_text_line, error_result_list)
						if(error_result_list[0] == 100):
							result_object =  query_by_coordinate_in_database[checked_database_list_element[0]](chr_number_string, user_start_int, user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string)
							if(result_object is None):
								print("[ Search out of region ]")
							else:
								result_object_list += result_object
				elif search_option == 'snpids':
					for input_text_line in input_text_list:
						result_object =  query_by_snp_in_database[checked_database_list_element[0]](input_text_line)
						if(result_object is None):
							print("[ No Result ]")
						else:
							result_object_list += result_object
				elif search_option == 'genes':
					for input_text_line in input_text_list:
						result_object =  query_by_gene_in_database[checked_database_list_element[0]](input_text_line)
						if(result_object is None):
							print("[ No Result ]")
						else:
							result_object_list += result_object

			hgmd_object_list = search_hgmd(result_object_list, checked_database_list_element[0])

			row_num = 0
			worksheet = workbook.add_worksheet(get_database_name(checked_database_list_element[0]))
			columns = get_columns_by_database[checked_database_list_element[0]]()

			format = workbook.add_format()
			for col_num in range(len(columns)):
				worksheet.set_column(col_num, col_num, columns[col_num][1])
				worksheet.write_string(row_num, col_num, columns[col_num][0], format)


			for result_object_data, hgmd_object_data in zip(result_object_list, hgmd_object_list):

				row_num += 1
				rows = get_rows_by_database[checked_database_list_element[0]](result_object_data, hgmd_object_data)
			
				for col_num in range(len(rows)):
					worksheet.write_string(row_num, col_num, str(rows[col_num]), format)


	elif _downloadStatus == 'current':
		result_object_list = []
		if search_option == 'coordinates':
			for input_text_line in input_text_list:
				error_result_list, chr_number_string, user_start_int, user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string = check_chromosome_position(input_text_line, error_result_list)
				if(error_result_list[0] == 100):
					result_object =  query_by_coordinate_in_database[_current_database](chr_number_string, user_start_int, user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string)
					if(result_object is None):
						print("[ Search out of region ]")
					else:
						result_object_list += result_object
		elif search_option == 'snpids':
			for input_text_line in input_text_list:
				result_object =  query_by_snp_in_database[_current_database](input_text_line)
				if(result_object is None):
					print("[ No Result ]")
				else:
					result_object_list += result_object
		elif search_option == 'genes':
			for input_text_line in input_text_list:
				result_object =  query_by_gene_in_database[_current_database](input_text_line)
				if(result_object is None):
					print("[ No Result ]")
				else:
					result_object_list += result_object

		hgmd_object_list = search_hgmd(result_object_list, _current_database)

		row_num = 0
		worksheet = workbook.add_worksheet(get_database_name(_current_database))
		columns = get_columns_by_database[_current_database]()

		format = workbook.add_format()
      
		for col_num in range(len(columns)):
			worksheet.set_column(col_num, col_num, columns[col_num][1])
			worksheet.write_string(row_num, col_num, columns[col_num][0], format)


		for result_object_data, hgmd_object_data in zip(result_object_list, hgmd_object_list):

			row_num += 1
			rows = get_rows_by_database[_current_database](result_object_data, hgmd_object_data)
			
			for col_num in range(len(rows)):
				worksheet.write_string(row_num, col_num, str(rows[col_num]), format)

	return response

def get_columns_by_database_one():
    return 	[
				(u"CHR", 7.2),
				(u"POS", 10.8),
				(u"SNP", 26.4),
				(u"cptid", 14.4),
				(u"STRAND", 13.2),
				(u"EFFECT_ALLELE", 18),
				(u"OTHER_ALLELE", 18),
				(u"EAF", 12),
				(u"IMPUTATION", 13.2),
				(u"all_maf", 12),
				(u"cases_maf", 12),
				(u"controls_maf", 14.4),
				(u"BETA", 12),
				(u"CIL", 12),
				(u"CIU", 12),
				(u"SE", 12),
				(u"PVAL", 12),
				(u"N", 12),
				(u"MAC", 12),
				(u"EAS.ref", 12),
				(u"RAF", 12),
				(u"GeneSymbols", 18),
				(u"UCSC IDs", 33.6),
				(u"Ensembl IDs", 60.3),
				(u"Description", 102),
				(u"Accession", 12),
				(u"Alt", 12),
				(u"Aminoacid Change", 16.8),
				(u"Citation Type", 12),
				(u"Codon Number", 14),
				(u"Comments", 30),
				(u"Confidence", 12),
				(u"Disease", 57.6),
				(u"Entrez", 12),
				(u"Genomic Sequence", 72),
				(u"Hgmd Acc", 12),
				(u"Hgvs", 33.6),
				(u"Icd-10", 42),
				(u"Lsdb Source", 12),
				(u"Mesh", 84),
				(u"Mutation Type", 13.2),
				(u"Nucleotide Change", 20.4),
				(u"Omim", 8.4),
				(u"Omim Ref", 64.8),
				(u"Pmid", 9.6),
				(u"Pmid Link", 45.6),
				(u"Pmid Notes", 14.4),
				(u"Ref", 66),
				(u"Rsid", 12),
				(u"Snomedct", 12),
				(u"Uniprot", 9.6),
				(u"Uniprot Link", 33.6),
				(u"Variant Type", 12)
	    ]

def get_columns_by_database_two():
    return [
			(u"Chromosome",13.2),
			(u"Start Position",13.2),
			(u"End Position",13.2),
			(u"Marker ID",36),
			(u"NS",18),
			(u"AC",12),
			(u"Call Rate",13.2),
			(u"maf",12),
			(u"pvalue",12),
			(u"BETA",12),
			(u"sebeta",12),
			(u"chisq",12),
			(u"ns_case",12),
			(u"ns_ctrl",12),
			(u"af_case",12),
			(u"af_ctrl",12),
			(u"Gene Symbols",18),
			(u"UCSC IDs",33.6),
			(u"Ensembl IDs",60.3),			
			(u"Description", 102),
			(u"Accession", 12),
			(u"Alt", 12),
			(u"Aminoacid Change", 16.8),
			(u"Citation Type", 12),
			(u"Codon Number", 14),
			(u"Comments", 30),
			(u"Confidence", 12),
			(u"Disease", 57.6),
			(u"Entrez", 12),
			(u"Genomic Sequence", 72),
			(u"Hgmd Acc", 12),
			(u"Hgvs", 33.6),
			(u"Icd-10", 42),
			(u"Lsdb Source", 12),
			(u"Mesh", 84),
			(u"Mutation Type", 13.2),
			(u"Nucleotide Change", 20.4),
			(u"Omim", 8.4),
			(u"Omim Ref", 64.8),
			(u"Pmid", 9.6),
			(u"Pmid Link", 45.6),
			(u"Pmid Notes", 14.4),
			(u"Ref", 66),
			(u"Rsid", 12),
			(u"Snomedct", 12),
			(u"Uniprot", 9.6),
			(u"Uniprot Link", 33.6),
			(u"Variant Type", 12)


    ]

get_columns_by_database = {
    "1" : get_columns_by_database_one,
    "2" : get_columns_by_database_two,
}


def get_rows_by_database_one(result_object_data, hgmd_object_data):
    if hgmd_object_data is not "NA":
    	return [
    				result_object_data["chr"],
    				result_object_data["pos"],
    				result_object_data["snp"],
    				result_object_data["cptid"],
    				result_object_data["strand"],
    				result_object_data["effect_allele"],
    				result_object_data["other_allele"],
    				result_object_data["eaf"],
    				result_object_data["imputation"],
    				result_object_data["all_maf"],
	    			result_object_data["cases_maf"],
    				result_object_data["controls_maf"],
    				result_object_data["beta"],
    				result_object_data["cil"],
    				result_object_data["ciu"],
    				result_object_data["se"],
    				result_object_data["pval"],
    				result_object_data["n"],
    				result_object_data["mac"],
    				result_object_data["eas_ref"],
	    			result_object_data["raf"],
    				result_object_data["genesymbols"],
    				result_object_data["ucsc_ids"],
    				result_object_data["ensembl_ids"],
					get_description_element('feature\=(.*?);', hgmd_object_data['description']),
    				get_description_element('accession\=(.*?);', hgmd_object_data['description']),
					get_description_element('alt\=(.*?);', hgmd_object_data['description']),
					get_description_element('aminoacid_change\=(.*?);', hgmd_object_data['description']),
					get_description_element('citation_type\=(.*?);', hgmd_object_data['description']),
					get_description_element('codon_number\=(.*?);', hgmd_object_data['description']),
					get_description_element('comments\=(.*?);', hgmd_object_data['description']),
					get_description_element('confidence\=(.*?);', hgmd_object_data['description']),
					get_description_element('disease\=(.*?);', hgmd_object_data['description']),
					get_description_element('entrez\=(.*?);', hgmd_object_data['description']),
					get_description_element('genomic_sequence\=(.*?);', hgmd_object_data['description']),
					get_description_element('hgmdAcc\=(.*?);', hgmd_object_data['description']),
					get_description_element('hgvs\=(.*?);', hgmd_object_data['description']),
					get_description_element('icd10\=(.*?);', hgmd_object_data['description']),
					get_description_element('lsdb_source\=(.*?);', hgmd_object_data['description']),
					get_description_element('mesh\=(.*?);', hgmd_object_data['description']),
					get_description_element('mutationType\=(.*?);', hgmd_object_data['description']),
					get_description_element('nucleotideChange\=(.*?);', hgmd_object_data['description']),
					get_description_element('omim\=(.*?);', hgmd_object_data['description']),
					get_description_element('omim_ref\=(.*?);', hgmd_object_data['description']),
					get_description_element('pmid\=(.*?);', hgmd_object_data['description']),
					change_arguments_into_url(get_description_element('pmid\=(.*?);', hgmd_object_data['description']), "pmid_link"),
					get_description_element('pmid_notes\=(.*?);', hgmd_object_data['description']),
					get_description_element('ref\=(.*?);', hgmd_object_data['description']),
					get_description_element('rsid\=(.*?);', hgmd_object_data['description']),
					get_description_element('snomedct\=(.*?);', hgmd_object_data['description']),
					get_description_element('uniprot\=(.*?);', hgmd_object_data['description']),
					change_arguments_into_url(get_description_element('uniprot\=(.*?);', hgmd_object_data['description']), "uniprot_link"),
					get_description_element('variantType\=(.*?);', hgmd_object_data['description']),
            	]
    else:
    	return [
    				result_object_data["chr"],
    				result_object_data["pos"],
    				result_object_data["snp"],
    				result_object_data["cptid"],
    				result_object_data["strand"],
    				result_object_data["effect_allele"],
    				result_object_data["other_allele"],
    				result_object_data["eaf"],
    				result_object_data["imputation"],
    				result_object_data["all_maf"],
	    			result_object_data["cases_maf"],
    				result_object_data["controls_maf"],
    				result_object_data["beta"],
    				result_object_data["cil"],
    				result_object_data["ciu"],
    				result_object_data["se"],
    				result_object_data["pval"],
    				result_object_data["n"],
    				result_object_data["mac"],
    				result_object_data["eas_ref"],
	    			result_object_data["raf"],
    				result_object_data["genesymbols"],
    				result_object_data["ucsc_ids"],
    				result_object_data["ensembl_ids"],
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA"
            	]

def get_rows_by_database_two(result_object_data, hgmd_object_data):
	if hgmd_object_data is not "NA":
		return [
    				result_object_data["chr"],
					result_object_data["start_pos"],
					result_object_data["end_pos"],
					result_object_data["marker_id"],
					result_object_data["ns"],
					result_object_data["ac"],
					result_object_data["callrate"],
					result_object_data["maf"],
					result_object_data["pvalue"],
					result_object_data["beta"],
					result_object_data["sebeta"],
					result_object_data["chisq"],
					result_object_data["ns_case"],
					result_object_data["ns_ctrl"],
					result_object_data["af_case"],
					result_object_data["af_ctrl"],
					result_object_data["genesymbols"],
					result_object_data["ucsc_ids"],
					result_object_data["ensembl_ids"],
					get_description_element('feature\=(.*?);', hgmd_object_data['description']),
    				get_description_element('accession\=(.*?);', hgmd_object_data['description']),
					get_description_element('alt\=(.*?);', hgmd_object_data['description']),
					get_description_element('aminoacid_change\=(.*?);', hgmd_object_data['description']),
					get_description_element('citation_type\=(.*?);', hgmd_object_data['description']),
					get_description_element('codon_number\=(.*?);', hgmd_object_data['description']),
					get_description_element('comments\=(.*?);', hgmd_object_data['description']),
					get_description_element('confidence\=(.*?);', hgmd_object_data['description']),
					get_description_element('disease\=(.*?);', hgmd_object_data['description']),
					get_description_element('entrez\=(.*?);', hgmd_object_data['description']),
					get_description_element('genomic_sequence\=(.*?);', hgmd_object_data['description']),
					get_description_element('hgmdAcc\=(.*?);', hgmd_object_data['description']),
					get_description_element('hgvs\=(.*?);', hgmd_object_data['description']),
					get_description_element('icd10\=(.*?);', hgmd_object_data['description']),
					get_description_element('lsdb_source\=(.*?);', hgmd_object_data['description']),
					get_description_element('mesh\=(.*?);', hgmd_object_data['description']),
					get_description_element('mutationType\=(.*?);', hgmd_object_data['description']),
					get_description_element('nucleotideChange\=(.*?);', hgmd_object_data['description']),
					get_description_element('omim\=(.*?);', hgmd_object_data['description']),
					get_description_element('omim_ref\=(.*?);', hgmd_object_data['description']),
					get_description_element('pmid\=(.*?);', hgmd_object_data['description']),
					change_arguments_into_url(get_description_element('pmid\=(.*?);', hgmd_object_data['description']), "pmid_link"),
					get_description_element('pmid_notes\=(.*?);', hgmd_object_data['description']),
					get_description_element('ref\=(.*?);', hgmd_object_data['description']),
					get_description_element('rsid\=(.*?);', hgmd_object_data['description']),
					get_description_element('snomedct\=(.*?);', hgmd_object_data['description']),
					get_description_element('uniprot\=(.*?);', hgmd_object_data['description']),
					change_arguments_into_url(get_description_element('uniprot\=(.*?);', hgmd_object_data['description']), "uniprot_link"),
					get_description_element('variantType\=(.*?);', hgmd_object_data['description']),
            	]
	else:
		return [
    				result_object_data["chr"],
					result_object_data["start_pos"],
					result_object_data["end_pos"],
					result_object_data["marker_id"],
					result_object_data["ns"],
					result_object_data["ac"],
					result_object_data["callrate"],
					result_object_data["maf"],
					result_object_data["pvalue"],
					result_object_data["beta"],
					result_object_data["sebeta"],
					result_object_data["chisq"],
					result_object_data["ns_case"],
					result_object_data["ns_ctrl"],
					result_object_data["af_case"],
					result_object_data["af_ctrl"],
					result_object_data["genesymbols"],
					result_object_data["ucsc_ids"],
					result_object_data["ensembl_ids"],
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA",
					"NA"
            	]
get_rows_by_database = {
    "1" : get_rows_by_database_one,
    "2" : get_rows_by_database_two,
}

def check_chromosome_position(_input_text_line, _error_result_list):
    error_code, chr_string, chr_number_string, user_start_int, user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string = 100, "", "", 0, 0, "", "", ""

    split_input_text_line_list = _input_text_line.split(':')
    genomic_chromosome_string, genomic_coordinate_string, ref_nucleotide_string, alt_nucleotide_string, gene_name_string = check_split_input_text_line_list(split_input_text_line_list)
    genomic_coordinate_string = genomic_coordinate_string.replace(",","")
    regex_search_result = re.search('(\d+)-(\d+)', genomic_coordinate_string)
    user_start_int = int(regex_search_result.group(1))
    user_end_int = int(regex_search_result.group(2))

    if user_start_int > user_end_int:
        _error_result_list[0] = 102
        _error_result_list[1].append(genomic_chromosome_string+":"+str(user_start_int)+"-"+str(user_end_int))
    elif user_end_int - user_start_int >= 1000000:
        _error_result_list[0] = 103
        _error_result_list[2].append(genomic_chromosome_string+":"+str(user_start_int)+"-"+str(user_end_int))
    else:
        _error_result_list[0] = 100;
    
    return _error_result_list, genomic_chromosome_string[3:], user_start_int, user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string

def check_split_input_text_line_list(_split_input_text_line_list):
    genomic_chromosome_string, genomic_coordinate_string, ref_nucleotide_string, alt_nucleotide_string, gene_name_string = _split_input_text_line_list[0], _split_input_text_line_list[1], "", "", ""

    if len(_split_input_text_line_list)==2:
        print("[ There are no any nucleotide symbol or gene name ]")
    elif len(_split_input_text_line_list)==3:
        if is_gene_name_string(_split_input_text_line_list[2]):
            gene_name_string = _split_input_text_line_list[2]
        else:
            print("The second input is gene name type >> ", _split_input_text_line_list[1])
    elif len(_split_input_text_line_list)==4:
        if is_nucleotide(_split_input_text_line_list[2]) and is_nucleotide(_split_input_text_line_list[3]):
            ref_nucleotide_string, alt_nucleotide_string = _split_input_text_line_list[2], _split_input_text_line_list[3]
        else:
            print("The second input is not nucleotide >> ", _split_input_text_line_list[2])
    elif len(_split_input_text_line_list)==5:
        if is_nucleotide(_split_input_text_line_list[2]) and is_nucleotide(_split_input_text_line_list[3]):
            ref_nucleotide_string, alt_nucleotide_string = _split_input_text_line_list[2], _split_input_text_line_list[3]
        elif is_gene_name_string(_split_input_text_line_list[2]):
            gene_name_string = _split_input_text_line_list[2]
        else:
            print("The second input are not nucleotide and gene name type >> ", _split_input_text_line_list[1])

        if is_nucleotide_string(_split_input_text_line_list[3]) and is_nucleotide_string(_split_input_text_line_list[4]):
            if ref_nucleotide_string == "" and alt_nucleotide_string == "":
                ref_nucleotide_string, is_nucleotide_string = _split_input_text_line_list[3], _split_input_text_line_list[4]
        elif is_gene_name_string(_split_input_text_line_list[4]):
            if gene_name_string == "":
                gene_name_string = _split_input_text_line_list[4]
        else:
            print("The input are not nucleotide or gene name type >> ", _split_input_text_line_list[2])
    else:
        print("Unexpected Error at check_split_input_text_line_list() >>", len(_split_input_text_line_list))
    return genomic_chromosome_string, genomic_coordinate_string, ref_nucleotide_string, alt_nucleotide_string, gene_name_string

def is_nucleotide(_input_string):
    nucleotide_string_list = ["A", "T", "G", "C"]

    for nucleotide_string in nucleotide_string_list:
        if nucleotide_string in _input_string:
            return True
    return False

def is_gene_name_string(_input_string):
    regex_search_result = re.search('([A-Z\d]+)', _input_string)
    if len(_input_string) == len(regex_search_result.group(1)):
        return True
    elif "orf" in _input_string:
        return True
    return False

def split_query_by_coordinate_in_database_one(chr_number_string, _user_start_int, _user_end_int, _ref_nucleotide_string, _alt_nucleotide_string, _gene_name_string):
	result_object = split_query_by_coordinate_in_database_one[chr_number_string](_user_start_int, _user_end_int, _ref_nucleotide_string, _alt_nucleotide_string, _gene_name_string)
	
	return result_object

def split_query_by_coordinate_in_database_two(chr_number_string, _user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_query_result = KobicExomeSequenceData.objects.filter(chr=chr_number_string, start_pos__gte = _user_start_int, end_pos__lte =_user_end_int).values()
	second_query_result = KobicExomeSequenceData.objects.filter(chr=chr_number_string, start_pos__lt = _user_start_int, end_pos__gte = _user_start_int, end_pos__lte = _user_end_int).values()
	third_query_result = KobicExomeSequenceData.objects.filter(chr=chr_number_string, start_pos__lte = _user_end_int, end_pos__gt = _user_end_int, start_pos__gte = _user_start_int).values()
	fourth_query_result = KobicExomeSequenceData.objects.filter(chr=chr_number_string, start_pos__lt = _user_start_int, end_pos__gt = _user_end_int).values()
	query_results = first_query_result | second_query_result | third_query_result | fourth_query_result
	distinct_query_results = query_results.distinct()
	if len(distinct_query_results.filter(marker_id__regex = r"_" + ref_nucleotide_string + "/" + alt_nucleotide_string + "_").values()) == 0:
		return distinct_query_results.filter(marker_id__regex = ref_nucleotide_string + "/" + alt_nucleotide_string).values()
	else:
		return distinct_query_results.filter(marker_id__regex = r"_" + ref_nucleotide_string + "/" + alt_nucleotide_string + "_").values()

query_by_coordinate_in_database = {
    "1" : split_query_by_coordinate_in_database_one,
    "2" : split_query_by_coordinate_in_database_two,
}

def split_query_in_chromosome_one_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr1.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_two_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr2.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_three_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr3.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_four_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr4.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_five_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr5.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_six_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr6.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_seven_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr7.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_eight_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr8.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_nine_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr9.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_ten_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr10.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_eleven_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr11.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_twelve_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr12.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_thirteen_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr13.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_fourteen_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr14.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_fifteen_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr15.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_sixteen_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr16.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_seventeen_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr17.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_eighteen_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr18.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_nineteen_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr19.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_twenty_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr20.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_twentyone_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr21.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

def split_query_in_chromosome_twentytwo_in_database_one(_user_start_int, _user_end_int, ref_nucleotide_string, alt_nucleotide_string, gene_name_string):
	first_result_object = KobicSnuhDiabetesChr22.objects.filter(pos__gte = _user_start_int, pos__lte = _user_end_int)
	second_result_object = first_result_object.filter(effect_allele__contains = alt_nucleotide_string, other_allele__contains = ref_nucleotide_string).values()
	return second_result_object

split_query_by_coordinate_in_database_one = {
    "1" : split_query_in_chromosome_one_in_database_one,
    "2" : split_query_in_chromosome_two_in_database_one,
    "3" : split_query_in_chromosome_three_in_database_one,
    "4" : split_query_in_chromosome_four_in_database_one,
    "5" : split_query_in_chromosome_five_in_database_one,
    "6" : split_query_in_chromosome_six_in_database_one,
    "7" : split_query_in_chromosome_seven_in_database_one,
    "8" : split_query_in_chromosome_eight_in_database_one,
    "9" : split_query_in_chromosome_nine_in_database_one,
    "10" : split_query_in_chromosome_ten_in_database_one,
    "11" : split_query_in_chromosome_eleven_in_database_one,
    "12" : split_query_in_chromosome_twelve_in_database_one,
    "13" : split_query_in_chromosome_thirteen_in_database_one,
    "14" : split_query_in_chromosome_fourteen_in_database_one,
    "15" : split_query_in_chromosome_fifteen_in_database_one,
    "16" : split_query_in_chromosome_sixteen_in_database_one,
    "17" : split_query_in_chromosome_seventeen_in_database_one,
    "18" : split_query_in_chromosome_eighteen_in_database_one,
    "19" : split_query_in_chromosome_nineteen_in_database_one,
    "20" : split_query_in_chromosome_twenty_in_database_one,
    "21" : split_query_in_chromosome_twentyone_in_database_one,
    "22" : split_query_in_chromosome_twentytwo_in_database_one,
}

def split_query_by_snp_in_database_one(input_text_line):
	result_object = KobicSnuhDiabetes.objects.filter(snp__search=input_text_line).values()
	return result_object

def split_query_by_snp_in_database_two(input_text_line):
	result_object = KobicExomeSequenceData.objects.filter(snp__search=input_text_line).values()
	return result_object

query_by_snp_in_database = {
    "1" : split_query_by_snp_in_database_one,
    "2" : split_query_by_snp_in_database_two,
}

def split_query_by_gene_in_database_one(input_text_line):
	result_object = KobicSnuhDiabetes.objects.filter(genesymbols__search=input_text_line).values()
	return result_object

def split_query_by_gene_in_database_two(input_text_line):
	result_object = KobicExomeSequenceData.objects.filter(genesymbols__search=input_text_line).values()
	return result_object

query_by_gene_in_database = {
    "1" : split_query_by_gene_in_database_one,
    "2" : split_query_by_gene_in_database_two,
}

def send_query_of_database_one_to_hgmd(_result_json_data_element):

	first_result_object = KobicHgmdData.objects.filter(chromosome = "chr"+str(_result_json_data_element["chr"]), feature_start = _result_json_data_element["pos"], feature_end = _result_json_data_element["pos"]).values()
	second_result_object = first_result_object.filter(description__contains = "ref=" + _result_json_data_element["other_allele"] +";").values()
	third_result_object = second_result_object.filter(description__contains = "alt=" + _result_json_data_element["effect_allele"] +";").values()
	
	return third_result_object

def send_query_of_database_two_to_hgmd(_result_json_data_element):
	marker_id = _result_json_data_element["marker_id"]
	marker_id_list = marker_id.split("_")
	ref = marker_id_list[1].split("/")[0]
	alt = marker_id_list[1].split("/")[1]
	first_result_object = KobicHgmdData.objects.filter(chromosome = "chr"+str(_result_json_data_element["chr"]), feature_start = _result_json_data_element["start_pos"], feature_end = _result_json_data_element["end_pos"])
	second_result_object = first_result_object.filter(description__contains = "ref=" + ref +";").values()
	third_result_object = second_result_object.filter(description__contains = "alt=" + alt +";").values()

	return third_result_object

send_query_to_hgmd = {
	"1" : send_query_of_database_one_to_hgmd,
	"2" : send_query_of_database_two_to_hgmd,
}