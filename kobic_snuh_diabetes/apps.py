from django.apps import AppConfig


class KobicSnuhDiabetesConfig(AppConfig):
    name = 'kobic_snuh_diabetes'
