from django.db import models
from django.forms import ModelForm
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.paginator import Paginator
from django.conf import settings

# Create your models here.
class InputCoordinatesText(models.Model):

    input_coordinates_text = models.TextField(blank=True)
    checked_database_list = models.TextField(blank=True)

class InputSnpidsText(models.Model):

    input_snpids_text = models.TextField(blank=True)
    checked_database_list = models.TextField(blank=True)

class InputGenesText(models.Model):

    input_genes_text = models.TextField(blank=True)
    checked_database_list = models.TextField(blank=True)

class InputCoordinatesTextForm(ModelForm):

    class Meta:
        model = InputCoordinatesText
        fields = ['input_coordinates_text', 'checked_database_list']

    def __init__(self, *args, **kwargs):
        super(InputCoordinatesTextForm, self).__init__(*args, **kwargs)
        self.fields['input_coordinates_text'].widget.attrs.update({'id': 'coordinatesSearchTextInput'})

class InputSnpidsTextForm(ModelForm):

    class Meta:
        model = InputSnpidsText
        fields = ['input_snpids_text', 'checked_database_list']

    def __init__(self, *args, **kwargs):
        super(InputSnpidsTextForm, self).__init__(*args, **kwargs)
        self.fields['input_snpids_text'].widget.attrs.update({'id': 'snpidsSearchTextInput'})

class InputGenesTextForm(ModelForm):

    class Meta:
        model = InputGenesText
        fields = ['input_genes_text', 'checked_database_list']

    def __init__(self, *args, **kwargs):
        super(InputGenesTextForm, self).__init__(*args, **kwargs)
        self.fields['input_genes_text'].widget.attrs.update({'id': 'genesSearchTextInput'})

class KobicExomeSequenceData(models.Model):
    id = models.IntegerField(primary_key=True)
    chr = models.CharField(db_column='CHR', max_length=40)  # Field name made lowercase.
    start_pos = models.IntegerField(db_column='START_POS')  # Field name made lowercase.
    end_pos = models.IntegerField(db_column='END_POS')  # Field name made lowercase.
    marker_id = models.CharField(db_column='MARKER_ID', max_length=1000)  # Field name made lowercase.
    ns = models.CharField(db_column='NS', max_length=40)  # Field name made lowercase.
    ac = models.CharField(db_column='AC', max_length=40)  # Field name made lowercase.
    callrate = models.CharField(db_column='CALLRATE', max_length=40)  # Field name made lowercase.
    maf = models.CharField(db_column='MAF', max_length=40)  # Field name made lowercase.
    pvalue = models.CharField(db_column='PVALUE', max_length=40)  # Field name made lowercase.
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    sebeta = models.CharField(db_column='SEBETA', max_length=40)  # Field name made lowercase.
    chisq = models.CharField(db_column='CHISQ', max_length=40)  # Field name made lowercase.
    ns_case = models.CharField(db_column='NS_CASE', max_length=40)  # Field name made lowercase.
    ns_ctrl = models.CharField(db_column='NS_CTRL', max_length=40)  # Field name made lowercase.
    af_case = models.CharField(db_column='AF_CASE', max_length=40)  # Field name made lowercase.
    af_ctrl = models.CharField(db_column='AF_CTRL', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=5000)  # Field name made lowercase.
    snp = models.CharField(db_column='snp', max_length=40)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_exome_sequence_data'

class KobicHgmdData(models.Model):
    chromosome = models.CharField(max_length=40)
    feature_start = models.IntegerField()
    feature_end = models.IntegerField()
    strand = models.CharField(max_length=10)
    description = models.CharField(db_column='DESCRIPTION', max_length=15000, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_hgmd_data'


class KobicSnuhDiabetes(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes'


class KobicSnuhDiabetesChr1(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr1'


class KobicSnuhDiabetesChr10(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr10'


class KobicSnuhDiabetesChr11(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr11'


class KobicSnuhDiabetesChr12(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr12'


class KobicSnuhDiabetesChr13(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr13'


class KobicSnuhDiabetesChr14(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr14'


class KobicSnuhDiabetesChr15(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr15'


class KobicSnuhDiabetesChr16(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr16'


class KobicSnuhDiabetesChr17(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr17'


class KobicSnuhDiabetesChr18(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr18'


class KobicSnuhDiabetesChr19(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr19'


class KobicSnuhDiabetesChr2(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr2'


class KobicSnuhDiabetesChr20(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr20'


class KobicSnuhDiabetesChr21(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr21'


class KobicSnuhDiabetesChr22(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr22'


class KobicSnuhDiabetesChr3(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr3'


class KobicSnuhDiabetesChr4(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr4'


class KobicSnuhDiabetesChr5(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr5'


class KobicSnuhDiabetesChr6(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr6'


class KobicSnuhDiabetesChr7(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr7'


class KobicSnuhDiabetesChr8(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr8'


class KobicSnuhDiabetesChr9(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr9'

class KobicSnuhDiabetesInputcoordinatestext(models.Model):
    input_coordinates_text = models.TextField()

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_inputcoordinatestext'
