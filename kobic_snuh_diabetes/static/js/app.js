(function()
{
	agGrid.initialiseAgGridWithAngular1(angular);

	var app = angular.module("kobic", ['kobic.Services', 'ngAnimate', 'ngCookies', 'agGrid']);


	app.controller("GlobalController", ['$scope', '$cookies', '$window', '$timeout',function($scope, $cookies, $rootScope, $timeout, $window, Services)
	{
		$scope.genesText = 'Enter or paste genes here';
		$scope.genesTextPlaceHolderValue = $scope.genesText;

		$scope.coordinatesText = 'Enter or paste genomic coordinates here';
		$scope.coordinatesTextPlaceHolderValue = $scope.coordinatesText;

		$scope.snpidsText = 'Enter or paste SNP IDs here';
		$scope.snpidsTextPlaceHolderValue = $scope.snpidsText;

		$scope.agGridTableElement = document.getElementById('agGridTable');
		$scope.agGridClass = document.getElementsByClassName('ag-dark')[0];

		$scope.databaseList = {};
		$scope.fullDatabaseList = {"1" : "GWAS+Imputation","2" : "Exome Sequence Data"};
	    $scope.currentDatabaseName = "";
	    $scope.downloadStatus = 'current';

    	var columnDefs = [];
    	var rowData = [];

	    this.databaseListFlag = false;

    	$scope.gridOptions = {
        rowData: rowData,
        rowHeight: $scope.agGridTableElement === null ? 0 : (window.innerHeight)/49,
      	enableColResize: true,
        suppressMovableColumns: true,
        onGridReady: function() 
        {
        		$scope.gridOptions.api.setColumnDefs(columnDefs);
	    }

	    };

	    this.initailizeBody = function()
	    {
	    	var inputWidth = (window.innerWidth < 940)? 940: window.innerWidth;
	    	var inputHeight = (window.innerHeight < 560)? 560: window.innerHeight;

	    	document.body.style.width = inputWidth + "px";
	    	document.body.style.height = inputHeight + "px";

	    	$scope.agGridClass.style.fontSize = 1*(inputHeight/1350) + "em";
	    }

	    this.initialiizeAgGrid = function(_current_database, result_json_data, hgmd_json_data)
	    {
	    	this.setColumnDefs(_current_database);
	    	this.setRawData(_current_database, result_json_data, hgmd_json_data);
	    }

	    this.setColumnDefs = function(_current_database)
	    {	
	    	switch(_current_database)
	    	{
				case 1:
					columnDefs= [
									{headerName: "CHR", field: "chr", editable: true, width: 60, pinned: 'left'},
									{headerName: "POS", field: "pos", editable: true, width: 90, pinned: 'left'},
									{headerName: "SNP", field: "snp", editable: true, width: 220, pinned: 'left'},
									{headerName: "cptid", field: "cptid", editable: true, width: 120},
									{headerName: "STRAND", field: "strand", editable: true, width: 110},
									{headerName: "EFFECT_ALLELE", field: "effect_allele", editable: true, width: 150},
									{headerName: "OTHER_ALLELE", field: "other_allele", editable: true, width: 150},
									{headerName: "EAF", field: "eaf", editable: true, width: 100},
									{headerName: "IMPUTATION", field: "imputation", editable: true, width: 110},
									{headerName: "all_maf", field: "all_maf", editable: true, width: 100},
									{headerName: "cases_maf", field: "cases_maf", editable: true, width: 100},
									{headerName: "controls_maf", field: "controls_maf", editable: true, width: 120},
									{headerName: "BETA", field: "beta", editable: true, width: 100},
									{headerName: "CIL", field: "cil", editable: true, width: 100},
									{headerName: "CIU", field: "ciu", editable: true, width: 100},
									{headerName: "SE", field: "se", editable: true, width: 100},
									{headerName: "PVAL", field: "pval", editable: true, width: 100},
									{headerName: "N", field: "n", editable: true, width: 100},
									{headerName: "MAC", field: "mac", editable: true, width: 100},
       	    						{headerName: "EAS.ref", field: "eas_ref", editable: true, width: 100},
       	    						{headerName: "RAF", field: "raf", editable: true, width: 100},
       	    						{headerName: "Gene Symbols", field: "genesymbols", editable: true, width: 150},
       	    						{headerName: "UCSC IDs", field: "ucsc_ids", editable: true, width: 280},
       	    						{headerName: "Ensembl IDs", field: "ensembl_ids", editable: true, width: 850},
       	    						{headerName: "Description", field: "description", editable: true, width: 530},
       	    						{headerName: "Accession", field: "accession", editable: true, width: 100},
       	    						{headerName: "Alt", field: "alt", editable: true, width: 100},
       	    						{headerName: "Aminoacid Change", field: "aminoacid_change", editable: true, width: 140},
       	    						{headerName: "Citation Type", field: "citation_type", editable: true, width: 100},
       	    						{headerName: "Codon Number", field: "codon_number", editable: true, width: 120},
       	    						{headerName: "Comments", field: "comments", editable: true, width: 250},
       	    						{headerName: "Confidence", field: "confidence", editable: true, width: 100},
       	    						{headerName: "Disease", field: "disease", editable: true, width: 480},
       	    						{headerName: "Entrez", field: "entrez", editable: true, width: 100},
       	    						{headerName: "Genomic Sequence", field: "genomic_sequence", editable: true, width: 600},
       	    						{headerName: "Hgmd Acc", field: "hgmdAcc", editable: true, width: 100},
       	    						{headerName: "Hgvs", field: "hgvs", editable: true, width: 380},
       	    						{headerName: "Icd-10", field: "icd10", editable: true, width: 350},
       	    						{headerName: "Lsdb Source", field: "lsdb_source", editable: true, width: 100},
       	    						{headerName: "Mesh", field: "mesh", editable: true, width: 700},
       	    						{headerName: "Mutation Type", field: "mutationType", editable: true, width: 110},
       	    						{headerName: "Nucleotide Change", field: "nucleotideChange", editable: true, width: 170},
       	    						{headerName: "Omim", field: "omim", editable: true, width: 70},
       	    						{headerName: "Omim Ref", field: "omim_ref", editable: true, width: 540},
       	    						{headerName: "Pmid", field: "pmid", editable: true, width: 80},
       	    						{headerName: "Pmid Link", field: "pmid_link", editable: true, width: 380},
       	    						{headerName: "Pmid Notes", field: "pmid_notes", editable: true, width: 120},
       	    						{headerName: "Ref", field: "ref", editable: true, width: 550},
       	    						{headerName: "Rsid", field: "rsid", editable: true, width: 100},
       	    						{headerName: "Snomedct", field: "snomedct", editable: true, width: 100},
       	    						{headerName: "Uniprot", field: "uniprot", editable: true, width: 80},
       	    						{headerName: "Uniprot Link", field: "uniprot_link", editable: true, width: 280},
       	    						{headerName: "Variant Type", field: "variantType", editable: true, width: 100},

								];
				break;

				case 2:
					columnDefs= [
									{headerName: "Chromosome", field: "chr", editable: true, width: 110, pinned: 'left'},
									{headerName: "Start Position", field: "start_pos", editable: true, width: 110, pinned: 'left'},
									{headerName: "Start Position", field: "end_pos", editable: true, width: 110, pinned: 'left'},
									{headerName: "Marker ID", field: "marker_id", editable: true, width: 300},
									{headerName: "NS", field: "ns", editable: true, width: 150},
									{headerName: "AC", field: "ac", editable: true, width: 100},
									{headerName: "Call Rate", field: "callrate", editable: true, width: 110},
									{headerName: "maf", field: "maf", editable: true, width: 100},
									{headerName: "pvalue", field: "pvalue", editable: true, width: 100},
									{headerName: "BETA", field: "beta", editable: true, width: 100},
									{headerName: "sebeta", field: "sebeta", editable: true, width: 100},
									{headerName: "chisq", field: "chisq", editable: true, width: 100},
									{headerName: "ns_case", field: "ns_case", editable: true, width: 100},
									{headerName: "ns_ctrl", field: "ns_ctrl", editable: true, width: 100},
									{headerName: "af_case", field: "af_case", editable: true, width: 100},
       	    						{headerName: "af_ctrl", field: "af_ctrl", editable: true, width: 100},
       	    						{headerName: "Gene Symbols", field: "genesymbols", editable: true, width: 150},
       	    						{headerName: "UCSC IDs", field: "ucsc_ids", editable: true, width: 280},
       	    						{headerName: "Ensembl IDs", field: "ensembl_ids", editable: true, width: 850},
       	    						{headerName: "Strand", field: "strand", editable: true, width: 60},
       	    						{headerName: "Description", field: "description", editable: true, width: 530},
       	    						{headerName: "Accession", field: "accession", editable: true, width: 100},
       	    						{headerName: "Alt", field: "alt", editable: true, width: 100},
       	    						{headerName: "Aminoacid Change", field: "aminoacid_change", editable: true, width: 140},
       	    						{headerName: "Citation Type", field: "citation_type", editable: true, width: 100},
       	    						{headerName: "Codon Number", field: "codon_number", editable: true, width: 120},
       	    						{headerName: "Comments", field: "comments", editable: true, width: 250},
       	    						{headerName: "Confidence", field: "confidence", editable: true, width: 100},
       	    						{headerName: "Disease", field: "disease", editable: true, width: 480},
       	    						{headerName: "Entrez", field: "entrez", editable: true, width: 100},
       	    						{headerName: "Genomic Sequence", field: "genomic_sequence", editable: true, width: 600},
       	    						{headerName: "Hgmd Acc", field: "hgmdAcc", editable: true, width: 100},
       	    						{headerName: "Hgvs", field: "hgvs", editable: true, width: 380},
       	    						{headerName: "Icd-10", field: "icd10", editable: true, width: 350},
       	    						{headerName: "Lsdb Source", field: "lsdb_source", editable: true, width: 100},
       	    						{headerName: "Mesh", field: "mesh", editable: true, width: 700},
       	    						{headerName: "Mutation Type", field: "mutationType", editable: true, width: 110},
       	    						{headerName: "Nucleotide Change", field: "nucleotideChange", editable: true, width: 170},
       	    						{headerName: "Omim", field: "omim", editable: true, width: 70},
       	    						{headerName: "Omim Ref", field: "omim_ref", editable: true, width: 540},
       	    						{headerName: "Pmid", field: "pmid", editable: true, width: 80},
       	    						{headerName: "Pmid Link", field: "pmid_link", editable: true, width: 380},
       	    						{headerName: "Pmid Notes", field: "pmid_notes", editable: true, width: 120},
       	    						{headerName: "Ref", field: "ref", editable: true, width: 550},
       	    						{headerName: "Rsid", field: "rsid", editable: true, width: 100},
       	    						{headerName: "Snomedct", field: "snomedct", editable: true, width: 100},
       	    						{headerName: "Uniprot", field: "uniprot", editable: true, width: 80},
       	    						{headerName: "Uniprot Link", field: "uniprot_link", editable: true, width: 280},
       	    						{headerName: "Variant Type", field: "variantType", editable: true, width: 100},
								];
				break;

				default:
					console.log("[ Unexpected Database Number at setColumnDefs() ]")
	    	}
	    }

	    this.setRawData = function(_current_database, _result_json_data, _hgmd_json_data)
	    {
	    	for (var index=0; index < _result_json_data.length; index++)
	    	{
		    	var singleRawObject = this.getSingleRawObject(_current_database, index, _result_json_data, _hgmd_json_data);
 	    		rowData.push(singleRawObject)
	    	}
	    }

	   	this.getSingleRawObject = function(_current_database, _index, _result_json_data, _hgmd_json_data)
	    {
	    	singleRawObject = new Object(); 

	    	switch(_current_database) 
	    	{
				case 1:
					singleRawObject["cptid"] = _result_json_data[_index].cptid;
					singleRawObject["snp"] = _result_json_data[_index].snp;
					singleRawObject["chr"] = _result_json_data[_index].chr;
					singleRawObject["pos"] = _result_json_data[_index].pos;
					singleRawObject["strand"] = _result_json_data[_index].strand;
					singleRawObject["effect_allele"] = _result_json_data[_index].effect_allele;
					singleRawObject["other_allele"] = _result_json_data[_index].other_allele;
					singleRawObject["eaf"] = _result_json_data[_index].eaf;
					singleRawObject["imputation"] = _result_json_data[_index].imputation;
					singleRawObject["all_maf"] = _result_json_data[_index].all_maf;
					singleRawObject["cases_maf"] = _result_json_data[_index].cases_maf;
					singleRawObject["controls_maf"] = _result_json_data[_index].controls_maf;
					singleRawObject["beta"] = _result_json_data[_index].beta;
					singleRawObject["cil"] = _result_json_data[_index].cil;
					singleRawObject["ciu"] = _result_json_data[_index].ciu;
					singleRawObject["se"] = _result_json_data[_index].se;
					singleRawObject["pval"] = _result_json_data[_index].pval;
					singleRawObject["n"] = _result_json_data[_index].n;
					singleRawObject["mac"] = _result_json_data[_index].mac;
					singleRawObject["eas_ref"] = _result_json_data[_index].eas_ref;
					singleRawObject["raf"] = _result_json_data[_index].raf;
					singleRawObject["genesymbols"] = _result_json_data[_index].genesymbols;
					singleRawObject["ucsc_ids"] = _result_json_data[_index].ucsc_ids;
					singleRawObject["ensembl_ids"] = _result_json_data[_index].ensembl_ids;
					singleRawObject["accession"] = this.getDescriptionElement(/accession\=(.*?);/, (_hgmd_json_data[_index].description == undefined? "NA" : _hgmd_json_data[_index].description))
					singleRawObject["alt"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/alt\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["aminoacid_change"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/aminoacid_change\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["citation_type"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/citation_type\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["codon_number"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/codon_number\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["comments"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/comments\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["confidence"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/confidence\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["disease"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/disease\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["entrez"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/entrez\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["genomic_sequence"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/genomic_sequence\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["hgmdAcc"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/hgmdAcc\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["hgvs"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/hgvs\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["icd10"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/icd10\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["lsdb_source"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/lsdb_source\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["mesh"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/mesh\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["mutationType"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/mutationType\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["nucleotideChange"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/nucleotideChange\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["omim"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/omim\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["omim_ref"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/omim_ref\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["pmid"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/pmid\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["pmid_link"] = _hgmd_json_data[_index].description == undefined? "NA" : this.changeArgumentsIntoUrl(singleRawObject["pmid"], "pmid_link");
					singleRawObject["pmid_notes"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/pmid_notes\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["ref"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/ref\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["rsid"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/rsid\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["snomedct"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/snomedct\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["uniprot"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/uniprot\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["uniprot_link"] = _hgmd_json_data[_index].description == undefined? "NA" : this.changeArgumentsIntoUrl(singleRawObject["uniprot"], "uniprot_link");
					singleRawObject["variantType"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/variantType\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["description"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/feature\=(.*?);/, _hgmd_json_data[_index].description);
				break;

				case 2:
					singleRawObject["chr"] = _result_json_data[_index].chr;
					singleRawObject["start_pos"] = _result_json_data[_index].start_pos;
					singleRawObject["end_pos"] = _result_json_data[_index].end_pos;
					singleRawObject["marker_id"] = _result_json_data[_index].marker_id;
					singleRawObject["ns"] = _result_json_data[_index].ns;
					singleRawObject["ac"] = _result_json_data[_index].ac;
					singleRawObject["callrate"] = _result_json_data[_index].callrate;
					singleRawObject["maf"] = _result_json_data[_index].maf;
					singleRawObject["pvalue"] = _result_json_data[_index].pvalue;
					singleRawObject["beta"] = _result_json_data[_index].beta;
					singleRawObject["sebeta"] = _result_json_data[_index].sebeta;
					singleRawObject["chisq"] = _result_json_data[_index].chisq;
					singleRawObject["ns_case"] = _result_json_data[_index].ns_case;
					singleRawObject["ns_ctrl"] = _result_json_data[_index].ns_ctrl;
					singleRawObject["af_case"] = _result_json_data[_index].af_case;
					singleRawObject["af_ctrl"] = _result_json_data[_index].af_ctrl;
					singleRawObject["genesymbols"] = _result_json_data[_index].genesymbols;
					singleRawObject["ucsc_ids"] = _result_json_data[_index].ucsc_ids;
					singleRawObject["ensembl_ids"] = _result_json_data[_index].ensembl_ids;
					singleRawObject["strand"] = _hgmd_json_data[_index].strand == undefined? "NA" : _hgmd_json_data[_index].strand;
					singleRawObject["accession"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/accession\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["alt"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/alt\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["aminoacid_change"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/aminoacid_change\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["citation_type"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/citation_type\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["codon_number"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/codon_number\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["comments"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/comments\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["confidence"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/confidence\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["disease"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/disease\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["entrez"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/entrez\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["genomic_sequence"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/genomic_sequence\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["hgmdAcc"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/hgmdAcc\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["hgvs"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/hgvs\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["icd10"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/icd10\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["lsdb_source"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/lsdb_source\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["mesh"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/mesh\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["mutationType"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/mutationType\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["nucleotideChange"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/nucleotideChange\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["omim"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/omim\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["omim_ref"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/omim_ref\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["pmid"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/pmid\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["pmid_link"] = _hgmd_json_data[_index].description == undefined? "NA" : this.changeArgumentsIntoUrl(singleRawObject["pmid"], "pmid_link");
					singleRawObject["pmid_notes"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/pmid_notes\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["ref"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/ref\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["rsid"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/rsid\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["snomedct"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/snomedct\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["uniprot"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/uniprot\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["uniprot_link"] = _hgmd_json_data[_index].description == undefined? "NA" : this.changeArgumentsIntoUrl(singleRawObject["uniprot"], "uniprot_link");
					singleRawObject["variantType"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/variantType\=(.*?);/, _hgmd_json_data[_index].description);
					singleRawObject["description"] = _hgmd_json_data[_index].description == undefined? "NA" : this.getDescriptionElement(/feature\=(.*?);/, _hgmd_json_data[_index].description);
				break;

				default:
					console.log("[ Unexpected Database Number at getSingleRawObject() ]")
	    	}
	    	return singleRawObject;
	    }


		this.clearGenesPlaceHolderValue = function()
		{
			$scope.genesTextPlaceHolderValue = '';
		}

		this.fillGenesPlaceHolderValue = function()
		{
			$scope.genesTextPlaceHolderValue = $scope.genesText;
		}

		this.clearCoordinatesPlaceHolderValue = function()
		{
			$scope.coordinatesTextPlaceHolderValue = '';
		}

		this.fillCoordinatesPlaceHolderValue = function()
		{
			$scope.coordinatesTextPlaceHolderValue = $scope.coordinatesText;
		}

		this.clearSnpidsPlaceHolderValue = function()
		{
			$scope.snpidsTextPlaceHolderValue = '';
		}

		this.fillSnpidsPlaceHolderValue = function()
		{
			$scope.snpidsTextPlaceHolderValue = $scope.snpidsText;
		}

		this.selectDeselectAll = function()
		{
			if(JSON.stringify($scope.databaseList) != JSON.stringify($scope.fullDatabaseList))
			{
				$scope.databaseList = {"1" : "GWAS+Imputation","2" : "Exome Sequence Data"};
			}
			else
			{
				$scope.databaseList = {};
			}
		}

		this.clickCheckBox = function(_index, _database_name)
		{
			if(!(_index in $scope.databaseList)) 
			{
				this.addToDatabaseList(_index, _database_name)
			}
			else if(_index in $scope.databaseList) 
			{
				this.removeFromList(_index)
			}
		}

		this.isChecked = function(_index)
		{
			if(!(_index in $scope.databaseList)) 
			{
				return false
			}
			else if(_index in $scope.databaseList) 
			{ 
				return true
			}			
		}

		this.addToDatabaseList = function(_index, _database_name)
		{
			$scope.databaseList[_index] = _database_name;
		}

 		this.removeFromList = function(_index)
		{
			delete $scope.databaseList[_index];
		}

		this.getCheckedDatabaseListJSON = function()
		{
			return JSON.stringify($scope.databaseList);
		}

	    this.setCurrentDatabase = function(_current_database)
	    {
	    	$scope.currentDatabaseName = _current_database;
	    }

	    this.toggleDatabaseList = function()
	    {
	    	this.databaseListFlag = !(this.databaseListFlag);
	    }

		this.getDatabaseListFlag = function()
	    {
	    	return this.databaseListFlag;
	    }

	    this.toggleDownloadStatus = function()
		{
			if($scope.downloadStatus=='all')
			{
				$scope.downloadStatus = 'current'	
			}
			else if($scope.downloadStatus=='current')
			{
				$scope.downloadStatus = 'all'	
			}
		}

		this.isDownloadStatusAll = function()
		{
			if($scope.downloadStatus=='all')
			{
				return true
			}
			else
			{
				return false
			}	

		}

		this.isDownloadStatusCurrent = function()
		{
			if($scope.downloadStatus=='current')
			{
				return true
			}
			else
			{
				return false
			}	

		}

		this.getDownloadStatus = function()
		{
			return $scope.downloadStatus
		}

		this.setDownloadStatus = function(_downloadStatus)
		{
			$scope.downloadStatus = _downloadStatus;
		}

	    this.getDescriptionElement = function(_regex, _target)
	    {
	    	regex_result = _target.match(_regex);
	    	if (regex_result != null)
	    	{
	    		return decodeURIComponent(regex_result[1]);
	    	} 
	    	else
	    	{
	    		return "NA"
	    	}
	    }

	    this.changeArgumentsIntoUrl = function(_target, _link_type)
	    {
	    	var tempString = "";
	    	var pubmedBasicUrl = "http://www.ncbi.nlm.nih.gov/pubmed/?term="
	    	var uniprotBasicUrl = "http://www.uniprot.org/uniprot/"

	    	splitTarget = _target.split(",");

	    	if(splitTarget[0]=='N/A')
	    	{
	    		return 'N/A'
	    	}

	    	if (_link_type == "pmid_link")
	    	{
	    		for ( index in splitTarget ) 
	    		{
		    		tempString += pubmedBasicUrl + splitTarget[index] + ", "
		    	}
	    	}
	    	else if (_link_type == "uniprot_link")
	    	{
	    		for ( index in splitTarget ) 
	    		{
		    		tempString += uniprotBasicUrl + splitTarget[index] + ", "
		    	}
		    }

	    	return tempString.slice(0,-2);
	    }


	}]);

})();