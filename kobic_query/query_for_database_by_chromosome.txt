CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr1 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr1 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '1';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr2 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr2 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '2';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr3 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr3 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '3';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr4 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr4 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '4';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr5 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr5 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '5';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr6 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr6 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '6';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr7 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr7 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '7';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr8 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr8 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '8';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr9 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr9 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '9';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr10 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr10 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '10';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr11 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr11 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '11';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr12 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr12 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '12';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr13 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr13 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '13';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr14 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr14 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '14';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr15 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr15 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '15';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr16 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr16 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '16';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr17 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr17 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '17';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr18 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr18 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '18';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr19 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr19 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '19';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr20 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr20 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '20';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr21 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr21 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '21';

CREATE TABLE kobic_schema.kobic_snuh_diabetes_chr22 LIKE kobic_schema.kobic_snuh_diabetes;
INSERT INTO kobic_schema.kobic_snuh_diabetes_chr22 SELECT * FROM kobic_schema.kobic_snuh_diabetes WHERE chr = '22';

