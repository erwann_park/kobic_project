# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class KobicExomeSequenceData(models.Model):
    id = models.IntegerField(primary_key=True)
    chr = models.CharField(db_column='CHR', max_length=40)  # Field name made lowercase.
    start_pos = models.IntegerField(db_column='START_POS')  # Field name made lowercase.
    end_pos = models.IntegerField(db_column='END_POS')  # Field name made lowercase.
    marker_id = models.CharField(db_column='MARKER_ID', max_length=1000)  # Field name made lowercase.
    ns = models.CharField(db_column='NS', max_length=40)  # Field name made lowercase.
    ac = models.CharField(db_column='AC', max_length=40)  # Field name made lowercase.
    callrate = models.CharField(db_column='CALLRATE', max_length=40)  # Field name made lowercase.
    maf = models.CharField(db_column='MAF', max_length=40)  # Field name made lowercase.
    pvalue = models.CharField(db_column='PVALUE', max_length=40)  # Field name made lowercase.
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    sebeta = models.CharField(db_column='SEBETA', max_length=40)  # Field name made lowercase.
    chisq = models.CharField(db_column='CHISQ', max_length=40)  # Field name made lowercase.
    ns_case = models.CharField(db_column='NS_CASE', max_length=40)  # Field name made lowercase.
    ns_ctrl = models.CharField(db_column='NS_CTRL', max_length=40)  # Field name made lowercase.
    af_case = models.CharField(db_column='AF_CASE', max_length=40)  # Field name made lowercase.
    af_ctrl = models.CharField(db_column='AF_CTRL', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=5000)  # Field name made lowercase.
    rsid = models.CharField(db_column='RSID', max_length=40)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_exome_sequence_data'


class KobicHgmdData(models.Model):
    chromosome = models.CharField(max_length=40)
    feature_start = models.IntegerField()
    feature_end = models.IntegerField()
    strand = models.CharField(max_length=10)
    description = models.CharField(db_column='DESCRIPTION', max_length=15000, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_hgmd_data'


class KobicSnuhDiabetes(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes'


class KobicSnuhDiabetesChr1(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr1'


class KobicSnuhDiabetesChr10(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr10'


class KobicSnuhDiabetesChr11(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr11'


class KobicSnuhDiabetesChr12(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr12'


class KobicSnuhDiabetesChr13(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr13'


class KobicSnuhDiabetesChr14(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr14'


class KobicSnuhDiabetesChr15(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr15'


class KobicSnuhDiabetesChr16(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr16'


class KobicSnuhDiabetesChr17(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr17'


class KobicSnuhDiabetesChr18(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr18'


class KobicSnuhDiabetesChr19(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr19'


class KobicSnuhDiabetesChr2(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr2'


class KobicSnuhDiabetesChr20(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr20'


class KobicSnuhDiabetesChr21(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr21'


class KobicSnuhDiabetesChr22(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr22'


class KobicSnuhDiabetesChr3(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr3'


class KobicSnuhDiabetesChr4(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr4'


class KobicSnuhDiabetesChr5(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr5'


class KobicSnuhDiabetesChr6(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr6'


class KobicSnuhDiabetesChr7(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr7'


class KobicSnuhDiabetesChr8(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr8'


class KobicSnuhDiabetesChr9(models.Model):
    cptid = models.CharField(primary_key=True, max_length=40)
    snp = models.CharField(db_column='SNP', max_length=40)  # Field name made lowercase.
    chr = models.IntegerField(db_column='CHR')  # Field name made lowercase.
    pos = models.IntegerField(db_column='POS')  # Field name made lowercase.
    strand = models.CharField(db_column='STRAND', max_length=40)  # Field name made lowercase.
    effect_allele = models.CharField(db_column='EFFECT_ALLELE', max_length=40)  # Field name made lowercase.
    other_allele = models.CharField(db_column='OTHER_ALLELE', max_length=40)  # Field name made lowercase.
    eaf = models.CharField(db_column='EAF', max_length=40)  # Field name made lowercase.
    imputation = models.CharField(db_column='IMPUTATION', max_length=40)  # Field name made lowercase.
    all_maf = models.CharField(max_length=40)
    cases_maf = models.CharField(max_length=40)
    controls_maf = models.CharField(max_length=40)
    beta = models.CharField(db_column='BETA', max_length=40)  # Field name made lowercase.
    cil = models.CharField(db_column='CIL', max_length=40)  # Field name made lowercase.
    ciu = models.CharField(db_column='CIU', max_length=40)  # Field name made lowercase.
    se = models.CharField(db_column='SE', max_length=40)  # Field name made lowercase.
    pval = models.CharField(db_column='PVAL', max_length=40)  # Field name made lowercase.
    n = models.CharField(db_column='N', max_length=40)  # Field name made lowercase.
    mac = models.CharField(db_column='MAC', max_length=40)  # Field name made lowercase.
    eas_ref = models.CharField(db_column='EAS_ref', max_length=40)  # Field name made lowercase.
    raf = models.CharField(db_column='RAF', max_length=40)  # Field name made lowercase.
    genesymbols = models.CharField(db_column='GeneSymbols', max_length=1000)  # Field name made lowercase.
    ucsc_ids = models.CharField(db_column='UCSC_IDs', max_length=1000)  # Field name made lowercase.
    ensembl_ids = models.CharField(db_column='Ensembl_IDs', max_length=3500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_chr9'


class KobicSnuhDiabetesInputcoordinatestext(models.Model):
    input_coordinates_text = models.TextField()
    checked_database_list = models.TextField()

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_inputcoordinatestext'


class KobicSnuhDiabetesInputgenestext(models.Model):
    input_genes_text = models.TextField()
    checked_database_list = models.TextField()

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_inputgenestext'


class KobicSnuhDiabetesInputsnpidstext(models.Model):
    input_snpids_text = models.TextField()
    checked_database_list = models.TextField()

    class Meta:
        managed = False
        db_table = 'kobic_snuh_diabetes_inputsnpidstext'
